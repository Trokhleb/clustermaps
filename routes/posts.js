const express = require('express');
const router = express.Router();

/* GET posts page. */
router.get('/', (req, res, next) => {
  res.send('/posts');
});

/* GET new post. */
router.get('/new', (req, res, next) => {
  res.send('/posts/new');
});

/* POST new post. */
router.post('/', (req, res, next) => {
  res.send('CREATE /posts');
});
/* POST new posts . */
router.get('/new/:id', (req, res, next) => {
  res.send('/posts/new');
});

router.post('/', (req, res, next) => {
  res.send('CREATE /posts');
})

router.get('/:id', (req, res, next) => {
  res.send('SHOW /post/:id');
})

router.put('/:id/edit', (req, res, next) => {
  res.send('EDIT /posts/:id/edit');
})

router.put('/:id', (req, res, next) => {
  res.send('UPDATE /posts/:id');
})


router.delete('/:id', (req, res, next) => {
  res.send('DELETE /posts/:id');
})


module.exports = router;
